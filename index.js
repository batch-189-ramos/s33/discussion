// console.log('Hello World')


// fetch is a way to retrieve data from an API. The '.then' function waits for the data to be fulfilled before it moves on to the other code.

/*let posts = fetch('https://jsonplaceholder.typicode.com/posts') //fetch function returns a promise that is consumable using the '.then' syntax as well.
	.then((response) => response.json())
	.then((json) => console.log(json))

console.log(posts)*/


// Async and Await is the Javascript ES6 equivalent of the '.then' syntax. It allows us to write more streamline/clean code while utilizing an API fetching functionality. It basically makes our code asynchronous.
async function fetchPosts(){
	let result = await fetch('https://jsonplaceholder.typicode.com/posts')
	console.log(result)

	let json = await result.json()

	console.log(json)
}

fetchPosts()


// Get a single post from the API
// let post = 
fetch('https:jsonplaceholder.typicode.com/posts/10')
	.then((response) => response.json())
	.then((data) => console.log(data))

// console.log(post);

// Create a singe post
/*
	We can specify the method using a second object argument within the fetch function. Besides the method we can also specify the headers and the body of the body of the request.
*/

fetch('https:jsonplaceholder.typicode.com/posts', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({ // no need to input _id/primary key 
		userId: 1,
		title: 'New Post',
		body: 'Hello World!'
	})
})
.then((response) => response.json())
.then((data) => console.log(data)) //this post was added on a temporary storage since this json placeholder site/server is for public use.


/*fetch('https:jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((data) => console.log(data))*/



// Update existing data from server/database using the PUT method
/*
	When updatingexisting data, you require 2 things:
	1. ID of the post to be updated
	2. New body/content of the post to be updated.
*/
fetch('https:jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated Post'
	})
})
.then((response) => response.json())
.then((data) => console.log(data))


// Delete a single post using the DELETE method
/*
	Make sure to specify the ID of the post to be deleted. No need for a header and body since we are not passing any data to this request.
*/
fetch('https:jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
	/*body: JSON.stringify({
		idsToBeDeleted: [1, 2, 6]
	})*/ //this is how you delete multiple ids or object from a server/database
})
.then((response) => response.json())
.then((data) => console.log(data))


// We can filter/query the URL using '?' syntax after the endpoint
fetch('https:jsonplaceholder.typicode.com/posts?userId=1')
.then((response) => response.json())
.then((data) => console.log(data))


// We can fetch the comments from a specific post by using the ID of the post as well as using the '/comments' endpoint
fetch('https:jsonplaceholder.typicode.com/posts/1/comments')
.then((response) => response.json())
.then((data) => console.log(data))






